<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="cms-block block-default block-<?php echo $code; ?>">
	<?php echo $content; ?>
</div>