SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE `cms_block` (
  `block_id` int(11) NOT NULL COMMENT 'Идентификатор блока',
  `code` varchar(255) NOT NULL COMMENT 'Код',
  `template` varchar(255) NOT NULL COMMENT 'Шаблон',
  `content` mediumtext NOT NULL COMMENT 'Содержимое',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время создания',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Время обновления',
  `is_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Включен / отключен'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `cms_page` (
  `page_id` int(11) NOT NULL COMMENT 'Идентификатор страницы',
  `url` varchar(255) NOT NULL COMMENT 'Короткий адрес',
  `title` varchar(255) NOT NULL COMMENT 'Название страницы',
  `template` varchar(255) NOT NULL COMMENT 'Шаблон',
  `header` varchar(255) DEFAULT NULL COMMENT 'Заголовок',
  `content` mediumtext COMMENT 'Содержимое',
  `meta_description` varchar(500) DEFAULT NULL COMMENT 'Мета описание',
  `meta_keywords` varchar(500) DEFAULT NULL COMMENT 'Мета ключевые слова',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время создания',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Время обновления',
  `is_active` tinyint(4) NOT NULL COMMENT 'Включена / отключена'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `cms_page` (`page_id`, `url`, `title`, `template`, `header`, `content`, `meta_description`, `meta_keywords`, `created_time`, `update_time`, `is_active`) VALUES
(1, 'home', 'Главная', '1-column', 'Добро пожаловать в наш интернет-магазин!', '<p>Здесь будет какой-то HTML-контент.</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima possimus asperiores esse, explicabo ipsa omnis, ea aperiam tenetur est ipsum expedita provident cumque quod vitae fugit aut iure amet, delectus, velit perspiciatis dolores hic fuga neque! Vero a repellendus eius blanditiis praesentium! Accusantium quam in voluptates tempora animi quos quasi, error ipsum necessitatibus quae natus.</p>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio ad odit est dolore laudantium explicabo odio natus, minus dolorum harum minima vitae porro, deserunt officia, ducimus labore architecto commodi. Aliquid enim commodi eum esse ducimus cum eveniet repudiandae praesentium debitis, quas maxime incidunt quasi atque sequi similique, adipisci nisi. At quis rerum, quae maiores. Labore sapiente sit, repudiandae quae minima commodi sequi facilis fugit quia deleniti facere, nihil maxime quisquam a magni reprehenderit atque praesentium dignissimos. Consequatur praesentium possimus et, corrupti magni, soluta reiciendis tenetur obcaecati itaque quibusdam.</p>', NULL, NULL, '2017-03-25 11:59:52', '2017-03-25 13:23:10', 1),
(2, 'about', 'О нас', '1-column', 'О нас', '<h2>Процесс обучения на практическом примере</h2>\r\n<p>Процесс обучения будет проходить гораздо проще, если полученные знания студенты смогу применить на практике. Именно поэтому учебный центр <strong>Всемирный ORT Днепр</strong> на своих курсах ставит практику в приоритет.</p>\r\n<p>Этот магазин - результат практической работы группы по PHP.</p>', NULL, NULL, '2017-03-25 12:10:44', '0000-00-00 00:00:00', 1),
(3, 'contacts', 'Контакты', 'contacts', 'Контактная информация', '<address>\r\n<ul>\r\n<li>49000, Украина, город Днепр</li>\r\n<li>ул. Шалом-Алейхема 4/26</li>\r\n<li>Культурно-деловой центр «Менора»</li>\r\n<li>11 этаж, офис 1105</li>\r\n</ul>\r\n</address>', NULL, NULL, '2017-03-25 12:10:44', '0000-00-00 00:00:00', 1),
(4, 'disabled-page', 'Отключенная страница', '1-column', 'Эта страница отключена', 'Эта страница отключена для демонстрации работы контроллера страниц. В дальнейшем её нужно удалить.', NULL, NULL, '2017-03-25 13:24:45', '0000-00-00 00:00:00', 0);

CREATE TABLE `core_config` (
  `config_id` int(11) NOT NULL COMMENT 'Идентификатор конфигурации',
  `code` varchar(255) NOT NULL COMMENT 'Код',
  `label` varchar(255) DEFAULT NULL COMMENT 'Заголовок',
  `value` mediumtext COMMENT 'Значение'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `core_config` (`config_id`, `code`, `label`, `value`) VALUES
(1, 'site_name', 'Название сайта', 'Autosho'),
(2, 'default_title', 'Заголовок по-молчанию', 'Autosho - самый несерьёзный интернет-магазин автомобилей'),
(3, 'default_keywords', 'Ключевые слова по-умолчанию', 'тут, должен, работать, сеошник'),
(4, 'default_description', 'Описание по-умолчанию', 'И тут тоже, желательно. должен поработать сеошник. а мы программисты. наше дело - код писать.');


ALTER TABLE `cms_block`
  ADD PRIMARY KEY (`block_id`),
  ADD UNIQUE KEY `code` (`code`);

ALTER TABLE `cms_page`
  ADD PRIMARY KEY (`page_id`),
  ADD KEY `url` (`url`);

ALTER TABLE `core_config`
  ADD PRIMARY KEY (`config_id`),
  ADD UNIQUE KEY `code` (`code`);


ALTER TABLE `cms_block`
  MODIFY `block_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор блока';
ALTER TABLE `cms_page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор страницы', AUTO_INCREMENT=5;
ALTER TABLE `core_config`
  MODIFY `config_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор конфигурации', AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
